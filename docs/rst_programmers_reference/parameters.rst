Parameter system
==========================

ParamDict
----------------------

.. automodule:: cbcpost.paramdict
    :members:
    :undoc-members:
    :show-inheritance:

Parameterized
-----------------------

.. automodule:: cbcpost.parameterized
    :members:
    :undoc-members:
    :show-inheritance:

