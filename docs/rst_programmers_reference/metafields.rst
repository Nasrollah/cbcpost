.. _metafields:

Implemented cbcpost.metafields
================================
.. automodule:: cbcpost.metafields
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Boundary`
----------------------

.. automodule:: cbcpost.metafields.Boundary
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`DomainAvg`
-----------------------

.. automodule:: cbcpost.metafields.DomainAvg
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`ErrorNorm`
-----------------------

.. automodule:: cbcpost.metafields.ErrorNorm
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Magnitude`
-----------------------

.. automodule:: cbcpost.metafields.Magnitude
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Maximum`
---------------------

.. automodule:: cbcpost.metafields.Maximum
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Minimum`
---------------------

.. automodule:: cbcpost.metafields.Minimum
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Norm`
------------------

.. automodule:: cbcpost.metafields.Norm
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`PointEval`
-----------------------

.. automodule:: cbcpost.metafields.PointEval
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`Restrict`
----------------------

.. automodule:: cbcpost.metafields.Restrict
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`SubFunction`
-------------------------

.. automodule:: cbcpost.metafields.SubFunction
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`TimeAverage`
-------------------------

.. automodule:: cbcpost.metafields.TimeAverage
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`TimeDerivative`
----------------------------

.. automodule:: cbcpost.metafields.TimeDerivative
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`TimeIntegral`
--------------------------

.. automodule:: cbcpost.metafields.TimeIntegral
    :members:
    :undoc-members:
    :show-inheritance:

