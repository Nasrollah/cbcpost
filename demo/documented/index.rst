.. _Demos:

Demos
==================================
To get started, we recommend starting with the demos. If you are
unfamiliar with FEniCS, please refer to the `FEniCS Tutorial <http://fenicsproject.org/documentation/tutorial/>`_
for the FEniCS-specifics of these demos.


**Documented demos**:

.. toctree::
    :maxdepth: 2

    Basic/documentation
    Restart/documentation
    Replay/documentation

